# Installation du theme HTML5Blank + Bootstrap
wp theme install https://github.com/capitaljigga/bootstrap-html5blank/archive/master.zip --activate

# Install Cookies Notice 
wp plugin install cookie-notice --activate
wp plugin install wordpress-seo --activate
wp plugin install advanced-custom-fields --activate
